﻿using ListCreator.ClassesFolder;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace ListCreator.DataTables
{
    public class ListObjectDB
    {
        //Create a readonly object to connect database
        readonly SQLiteAsyncConnection _database;

        //No default constructor, only use constructor that takes in the path as a string
        public ListObjectDB(string path)
        {
            //Create the connection to the path
            _database = new SQLiteAsyncConnection(path);
            //Create the table for ListObject
            _database.CreateTableAsync<ListObject>().Wait();
        }

        //Create a table for a ItemModel Child
        public void CreateEntertainmentTable()
        {
            _database.CreateTableAsync<EntertainmentItem>().Wait();
        }


        #region ListObject table methods
        //Get to return all the data as a list
        public async Task<List<ListObject>> GetListObjects()
        {
            return await _database.Table<ListObject>().ToListAsync();
        }

        //Get a specific listObject from id
        public async Task<ListObject> GetObjectFromId(int id)
        {
            return await _database.Table<ListObject>()
                .Where(i => i.id == id).FirstOrDefaultAsync();
        }

        //Add a new listobject to the db
        public async Task<int> AddListObject(ListObject obj)
        {
            return await _database.InsertAsync(obj);
        }

        //Update list object in db
        public async Task<int> UpdateListObject(ListObject obj)
        {
            return await _database.UpdateAsync(obj);
        }

        //Delete an object
        public async Task<int> DeleteListObject(ListObject obj)
        {
            return await _database.DeleteAsync(obj);
        }

        //Delete all object 
        public async Task<int> DeleteAllLists()
        {
            //This should also delete all tables that have the same ids as this listObjects ids
            return await _database.DeleteAllAsync<ListObject>();
        }
        #endregion


        #region EntertainmentItem methods


        //Get a specific itemmodels from an id
        public async Task<EntertainmentItem> GetItemModel(int id)
        {
            return await _database.Table<EntertainmentItem>().Where(m => m.id == id).FirstOrDefaultAsync();
        }


        //Get to return list of entertainment type
        async public Task<List<EntertainmentItem>> GetEntertainmentTypeList(String entType)
        {
            return await _database.Table<EntertainmentItem>().Where(m => m.entType == entType).ToListAsync();
        }

        //Add a new itemmodel to the list
        public async Task<int> AddItemModel(EntertainmentItem IM)
        {
            return await _database.InsertAsync(IM);
        }

        #endregion
    }
}
