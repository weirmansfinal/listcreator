﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ListCreator
{
    class FileNames
    {
        //String to hold enviroment file paht
        String filePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        //File name
        String fileName = "listNames.json";
        HashSet<String> listNames;

        public FileNames()
        {
            //Make the full path
            String fullPath = Path.Combine(filePath, fileName);

            //Read in list names from the file
            //set list to that list
            if (!File.Exists(fullPath))
            {
                //Create the file and list
                File.Create(fullPath);
                listNames = new HashSet<string>();
            }
            else
            {
                //Read in the file set the list to the read in data
            }
        }
    }
}
