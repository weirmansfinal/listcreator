﻿using ListCreator.DataTables;
using System;
using System.IO;
using Xamarin.Forms;

namespace ListCreator
{
    public partial class App : Application
    {

        //Set up static db object, similar to how a singleton object is set up
        private static ListObjectDB database;

        //This is the db for the whole app all tables will exist with this db
        public static ListObjectDB Database
        {
            get
            {
                if (database == null)
                {
                    database = new ListObjectDB(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ListObjects.db3"));
                }
                return database;
            }
        }
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
