﻿using ListCreator.ClassesFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;

namespace ListCreator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        //Want this so there is no duplicate lists
        List<ListObject> listsNames;
        public MainPage()
        {
            InitializeComponent();


            //Initialize list so that null doesnt occur later down the road
            listsNames = new List<ListObject>();
            this.Title = "List Manager";

            //Add a toolbar item on the right side
            this.ToolbarItems.Add(new ToolbarItem("CreateList", "filter.png", async () =>
            {
                var CreateObjectPage = new CreateListObjectPage(this);
                await Navigation.PushModalAsync(CreateObjectPage);
            }
            ));



            //Subscribe to method to present a new screen when tapped
            ListsName.ItemTapped += ListsName_ItemTapped;

            //Refreshing event
            ListsName.Refreshing += ListsName_Refreshing;

            ListsName.IsPullToRefreshEnabled = true;

        }

        async private void ListsName_Refreshing(object sender, EventArgs e)
        {
            listsNames = await App.Database.GetListObjects();
            ListsName.ItemsSource = listsNames;
            ListsName.IsRefreshing = false;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            //Read in list from sqlite file, assign it to list 
            listsNames = await App.Database.GetListObjects();

            //Set the list of names to the source so it appears on screen
            ListsName.ItemsSource = listsNames;
        }



        //Send list name and id to new controller so we can query file for right data
        async private void ListsName_ItemTapped(object sender, ItemTappedEventArgs e)
        {
           
            var item = (ListObject)e.Item;

            
            //get row id and name so we can pass it to the page
            ListContentPage lcp = new ListContentPage(item.listType);

            //Push the new lcp onto the stack and present it
            await Navigation.PushAsync(lcp);
        }

        public void GetInfo(object sender, EventArgs e)
        {
            //Either sends user to a modal page or shows an alert with all the info of the list
        }

        async public void Delete(object sender, EventArgs e)
        {
            var mi = (MenuItem)sender;
            //Show an alert and have the user click delete to make sure they want to
            bool alert = await DisplayAlert("Delete List", "Are you sure you want to delete this list?", "Delete", "No");

            //Check what the user clicked on delete is true and no is false
            if (alert)
            {
                //Perform delete action
                ListObject RowItem = (ListObject)mi.CommandParameter;

                //call the db delte action
                var response = await App.Database.DeleteListObject(RowItem);


                if (response == 0)
                {
                    //bad response
                    //Display an alert telling the user
                    await DisplayAlert("Problem", "There was a problem deleting this List", "Okay");
                }
                else
                {
                    //good response
                    ListsName.BeginRefresh();
                    ListsName.EndRefresh();
                }
            }
        }

        #region contentPage class
        private class CreateListObjectPage : ContentPage
        {
            Button Create;
            Button Cancel;
            Label nameLabel;
            Label typeLabel;
            Label subTypeLabel;
            Entry ListName;
            Picker ThemePicker;
            Picker SubThemePicker;
            ListObject obj;
            Grid grid;
            StackLayout leftStack;
            StackLayout rightStack;


            //Need a list of Strings to hold the themes of the list
            List<String> themes;
            //For subStrings we need a list to hold lists of subthemes so the binding process is easy
            //This goes on the assumption that there is a consists between theme and sub theme, entertainment in 0 of theme and its 
            //Sub themes in the main list of subthemes
            List<List<String>> EntertainmentTypes;

            public CreateListObjectPage(MainPage mp)
            {
                MainPage mainpage = mp;
                //Set up listObject
                obj = new ListObject();
                grid = new Grid();
                leftStack = new StackLayout();
                rightStack = new StackLayout();

                //Set up list and add themes we want
                themes = new List<string>();
                themes.Add("Entertainment");

                //Set up subthemes table
                EntertainmentTypes = new List<List<string>>();
                //Set up the lists inside of the list
                EntertainmentTypes.Add(new List<string>());
                EntertainmentTypes[0].Add("Books");
                EntertainmentTypes[0].Add("Movies");
                EntertainmentTypes[0].Add("Video Games");
                EntertainmentTypes[0].Add("Board Games");
                EntertainmentTypes[0].Add("Shows");

                ListName = new Entry
                {

                };

                #region label Creation
                //Create labels and assign text to them
                nameLabel = new Label
                {
                    Text = "Name:",
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.End
                };
                typeLabel = new Label
                {
                    Text = "Theme:",
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.End
                };
                subTypeLabel = new Label
                {
                    Text = "Sub-Themes",
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.End
                };
                #endregion

                #region picker Creation
                //Set up picker with title and set item source
                ThemePicker = new Picker
                {
                    Title = "Themes",
                    TitleColor = Color.Aqua
                };
                ThemePicker.ItemsSource = themes;
                SubThemePicker = new Picker
                {
                    Title = "Sub-Themes",
                    TitleColor = Color.Aqua
                };
                //Set to false so that the user has to pick a Theme in order ot get to subtheme menu
                SubThemePicker.IsEnabled = false;
                //Bind the picker choice to what array is in the picker for subtheme
                ThemePicker.SelectedIndexChanged += ThemePicker_SelectedIndexChanged;
                ThemePicker.SelectedIndex = 0;
                SubThemePicker.SelectedIndex = 0;
                #endregion

                #region button stuff
                //Create a new button set its text and vert and horz.
                Create = new Button
                {
                    Text = "Create List",
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.Center
                };
                Create.Clicked += async (sender, args) =>
                {
                    //set all items that we need for the obj befoer its added
                    obj.listName = ListName.Text;
                    obj.listType = (string)SubThemePicker.SelectedItem;
                    
                    


                    //do a quick query to see if a list of that subquery already exist
                    List<ListObject> temp = await App.Database.GetListObjects();
                    ListObject tempObj = new ListObject();

                    if (obj.listName != "" && ((String) ThemePicker.SelectedItem == "Entertainment"))
                    {
                        //Add list object to Table ListObjects
                        await App.Database.AddListObject(obj);
                        //Create a table for the child this obj is
                        App.Database.CreateEntertainmentTable();
                    }
                    else
                    {
                        //tell the user what happened
                    }

                    await Navigation.PopModalAsync();

                };

                //Create the cancel button that just removes this view controller from the stack
                Cancel = new Button
                {
                    Text = "Cancel",
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.Center
                };
                Cancel.Clicked += async (sender, args) =>
                {
                    await Navigation.PopModalAsync();
                };
                #endregion

                #region grid creation and adding
                //Set up grid 3x2
                grid.RowDefinitions.Add(new RowDefinition
                {
                    Height = new GridLength(2, GridUnitType.Auto)
                });
                grid.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(2, GridUnitType.Auto)
                });
                grid.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(2, GridUnitType.Auto)
                });

                leftStack.Children.Add(nameLabel);
                leftStack.Children.Add(typeLabel);
                leftStack.Children.Add(subTypeLabel);
                leftStack.Children.Add(Cancel);
                rightStack.Children.Add(ListName);
                rightStack.Children.Add(ThemePicker);
                rightStack.Children.Add(SubThemePicker);
                rightStack.Children.Add(Create);

                grid.Children.Add(rightStack, 1, 0);
                grid.Children.Add(leftStack, 0, 0);

                grid.HorizontalOptions = LayoutOptions.CenterAndExpand;
                grid.VerticalOptions = LayoutOptions.CenterAndExpand;
                grid.BackgroundColor = new Color(0, 0, 0, 0.5); //semi transparent background
                #endregion

                this.Content = grid;
            }

            //Should have the subtheme list change to the correct one
            private void ThemePicker_SelectedIndexChanged(object sender, EventArgs e)
            {
                Picker p = (Picker)sender;
                int selected = p.SelectedIndex;
                SubThemePicker.ItemsSource = EntertainmentTypes[selected];
                SubThemePicker.IsEnabled = true;
            }
        }
        #endregion

    }
}
