﻿using SQLite;
using System;

namespace ListCreator.ClassesFolder
{
    public class EntertainmentItem
    {
        //Every item no matter the list theme will have these things
        //Id for the element in the list
        [PrimaryKey]
        public int id { get; }

        //Name of the element
        public String elementName { get; set; }

        //Type of entertainment
        public String entType { get; set; }

        //Entertainment Genre
        public String entGenre { get; set; }

        //Description of the entertainment
        public String description { get; set; }

        //Boolean to see if ive watched, read, or played 
        public bool readWatchedPlayed { get; set; }



        //Default contructor should create a new id and blank string
        public EntertainmentItem()
        {
            Random r = new Random();
            id = r.Next();
            elementName = "";
            entType = "";
            entGenre = "";
            description = "";
            readWatchedPlayed = false;
        }

    }
}
