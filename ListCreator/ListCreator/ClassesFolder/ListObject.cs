﻿using SQLite;
using System;

namespace ListCreator.ClassesFolder
{
    public class ListObject
    {

        //Needs to store the id name of list and list's objects
        [PrimaryKey]
        public int id { get; set; }
        //String to hold the name of the list
        public String listName { get; set; }
        // set to store items in 
        public String listType { get; set; }
        //Will just be set once with when it was created
        public DateTime DateCreated { get; }

        //Will be edited every time an item is edited or added/removed
        public DateTime DateEdited { get; set; }

        //Int to store number of items in the list
        public int numObjects { get; set; }

        //Default constructor,
        //Get current time and set objects
        public ListObject()
        {
            Random r = new Random();
            id = r.Next();
            listName = "";
            listType = "";
            DateCreated = DateTime.Now;
            DateEdited = DateTime.Now;
            numObjects = 0;
        }

    }
}
