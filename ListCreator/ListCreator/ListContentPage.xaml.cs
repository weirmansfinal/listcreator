﻿using ListCreator.ClassesFolder;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListCreator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListContentPage : ContentPage
    {
        String Theme;
        List<EntertainmentItem> ModelList;
        public ListContentPage(String subTheme)
        {
            InitializeComponent();
            Theme = subTheme;

            //Add title and nav bar item
            this.Title = subTheme + " List";

            this.ToolbarItems.Add(new ToolbarItem("Add " + subTheme , null, async () =>
            {
                //Create a new page object to fill in the form then push it modally
                //Right nwo just make a new Movie object and add it to the list
                //Issue converting from child to parent
                //await App.Database.AddItemModel(m);
            }));

            //Set up refresh functionality of this page
            ItemModelList.Refreshing += ItemModelList_Refreshing;
            ItemModelList.IsPullToRefreshEnabled = true;
        }

        async private void ItemModelList_Refreshing(object sender, EventArgs e)
        {
            ModelList = await App.Database.GetEntertainmentTypeList(Theme);
            ItemModelList.ItemsSource = ModelList;
            ItemModelList.IsRefreshing = false;
        }

        async protected override void OnAppearing()
        {
            base.OnAppearing();

            ModelList = await App.Database.GetEntertainmentTypeList(Theme);

            ItemModelList.ItemsSource = ModelList;
        }
    }
}